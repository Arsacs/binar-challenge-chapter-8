import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import CreatePlayer from './createPlayer'
import EditPlayer from './editPlayer'
import SearchPlayer from './searchPlayer'


import React, { Component } from 'react'

export default class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">        
          <Switch>
            <Route path="/edit">
              <EditPlayer />
            </Route>
            <Route path="/search">
              <SearchPlayer />
            </Route>
            <Route path="/">
              <CreatePlayer/>
            </Route>
          </Switch>
        </div>
      </Router>
    )
  }
}
